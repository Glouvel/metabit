#!/usr/bin/env python

#from __future__ import print_function

"""
Main file performing the metaBIT pipeline steps.
"""


import sys
import time
import pysam
import logging
import os.path
import tempfile
import subprocess


# paleomix pypeline modules
import pypeline.yaml
import pypeline.logger
from   pypeline.pipeline       import Pypeline
from pypeline.common.console import print_err, print_info


# metaBIT modules
import parts
from makefile import MakefileError, read_makefiles
from config import parse_config, __version__


_tools = {"filterMetaphlan":  ["nodes", "tools", "filterMetaphlan.py"],
          "get_stats":        ["nodes", "tools", "get_stats.py"],
          "metaphlan2krona":  ["nodes", "tools", "metaphlan2krona_2.py"],
          "reformat_taxa":    ["nodes", "tools", "reformat_taxa.py"],
          "doPcoa":           ["nodes", "tools", "statax_Rmodule", "doPcoa.R"],
          "doClust":          ["nodes", "tools", "statax_Rmodule", "doClust.R"],
          "doDiv":            ["nodes", "tools", "statax_Rmodule", "doDiv.R"],
          "doHeatmap":        ["nodes", "tools", "statax_Rmodule", "doHeatmap.R"],
          "doBarplot":        ["nodes", "tools", "statax_Rmodule", "doBarplot.R"]}


def run(config, args):
    logfile_template = time.strftime("metaBIT.%Y%m%d_%H%M%S_%%02i.log")
    pypeline.logger.initialize(config, logfile_template)
    logger = logging.getLogger(__name__)
    try:
        logger.info("Building metaBIT pipeline ...")
        makefiles = read_makefiles(args, config)
    except (MakefileError, pypeline.yaml.YAMLError, IOError), error:
        print_err("Error reading makefiles:",
                  "\n  %s:\n   " % (error.__class__.__name__,),
                  "\n    ".join(str(error).split("\n")),
                  file=sys.stderr)
        return 1

    # setups for the logfile
    pipeline = Pypeline(config=config)

    for makefile in makefiles:
        if not makefile.get('run_from_table'):
            rmdup_nodes, \
            metaphlan_nodes, \
            summary_node = parts.profiling(makefile, config)
            
            pipeline.add_nodes(rmdup_nodes, summary_node)
            pipeline.add_nodes(parts.analyzing(makefile, config, metaphlan_nodes))
        else:
            pipeline.add_nodes(parts.analyzing(makefile, config))

    if config.list_output_files:
        logger.info("Printing output files ...")
        pipeline.print_output_files()
        return 0
    elif config.list_executables:
        logger.info("Printing required executables ...")
        pipeline.print_required_executables()
        return 0

    logger.info("MetaBIT running now!")

    if not pipeline.run(dry_run=config.dry_run,
                        max_running=config.max_threads,
                        progress_ui=config.progress_ui):
        return 1

    return 0 


def _print_usage(script):
    basename = os.path.basename(script)

    print_info("METAGENOMICS Pipeline %s\n" % (__version__,))
    print_info("Usage:")
    print_info("  ./%s makefile.yaml" % basename)


def main(argv):

    arg1 = sys.argv[1]

    if arg1 in _tools:
        path = os.path.dirname(os.path.realpath(__file__))
        command = os.path.join( *[path] + _tools[arg1])
        return subprocess.call([command] + sys.argv[2:])

    config, args = parse_config(argv[1:])
    if not args:
        _print_usage(argv[0])
        print_err("\nPlease specify one makefile!\n")
        return 1
   
    return run(config, args)


if __name__ == '__main__':
    sys.exit(main(sys.argv))
