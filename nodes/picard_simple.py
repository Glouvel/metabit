#!/usr/bin/python
#
# Copyright (c) 2012 Mikkel Schubert <MSchubert@snm.ku.dk>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Slightly modified version of Mikkel Schubert's picard node


import os
import getpass

from pypeline.node import CommandNode
from pypeline.atomiccmd.builder import \
    AtomicJavaCmdBuilder, \
    create_customizable_cli_parameters, \
    use_customizable_cli_parameters
from pypeline.common.fileutils import \
    swap_ext, \
    try_rmtree,\
    try_remove, \
    reroot_path, \
    describe_files
from pypeline.common.utilities import \
    safe_coerce_to_tuple
import pypeline.common.versions as versions


def _picard_version(config, jar_file):
    if jar_file not in _PICARD_VERSION_CACHE:
        params = AtomicJavaCmdBuilder(jar_file,
                                      temp_root=config.temp_root,
                                      jre_options=config.jre_options)
        params.add_value("--version")

        name = "Picard " + os.path.basename(jar_file)
        requirement = versions.Requirement(call=params.finalized_call,
                                           name=name,
                                           search=r"^(\d+)\.(\d+)",
                                           checks=versions.GE(1, 82))
        _PICARD_VERSION_CACHE[jar_file] = requirement
    return _PICARD_VERSION_CACHE[jar_file]
_PICARD_VERSION_CACHE = {}


class PicardNode(CommandNode):
    """Base class for nodes using Picard Tools; adds an additional cleanup
    step, in order to allow the jars to be run using the same temporary folder
    as any other commands associated with the node. This is necessary as some
    Picard tools create a large number of temporary files, leading to potential
    performance issues if these are located in the same folder.
    """

    def _teardown(self, config, temp):
        # Picard creates a folder named after the user in the temp-root
        try_rmtree(os.path.join(temp, getpass.getuser()))
        # Some JREs may create a folder for temporary performance counters
        try_rmtree(os.path.join(temp, "hsperfdata_" + getpass.getuser()))

        CommandNode._teardown(self, config, temp)


class MarkDuplicatesNode(PicardNode):
    @create_customizable_cli_parameters
    def customize(cls, config, input_bams, output_bam, output_metrics=None,
                  keep_dupes=False, dependencies=()):
        jar_file = os.path.join(config.jar_root, "MarkDuplicates.jar")
        params = AtomicJavaCmdBuilder(jar_file, jre_options=config.jre_options)

        input_bam = safe_coerce_to_tuple(input_bams)[0]
        # Create .bai index, since it is required by a lot of other programs
        #params.set_option("CREATE_INDEX", "True", sep="=")

        params.set_option("OUTPUT", "%(OUT_BAM)s", sep="=")
        params.set_option("METRICS_FILE", "%(OUT_METRICS)s", sep="=")
        #params.add_multiple_options("I", input_bams, sep="=")
        params.set_option("I", "%(IN_BAM)s", sep="=")
        ## testing
        # params.set_option("MAX_FILE_HANDLES_FOR_READ_ENDS_MAP", "16000", sep="=") 
        # params.set_option("MAX_RECORDS_IN_RAM", "1000000", sep="=") 
        # params.set_option("MAX_SEQUENCES_FOR_DISK_READ_ENDS_MAP", "100000", sep="=") 

        if not keep_dupes:
            # Remove duplicates from output by default to save disk-space
            params.set_option("REMOVE_DUPLICATES", "True",
                              sep="=", fixed=False)

        output_metrics = output_metrics or swap_ext(output_bam, ".metrics")
        params.set_kwargs(IN_BAM=input_bam,
                          OUT_BAM=output_bam,
        #                  OUT_BAI=swap_ext(output_bam, ".bai"),
                          OUT_METRICS=output_metrics,
                          CHECK_JAR=_picard_version(config, jar_file))

        return {"command": params,
                "dependencies": dependencies,
                "input_bam": input_bam}

    @use_customizable_cli_parameters
    def __init__(self, parameters):
        description = "<MarkDuplicates: %s>" \
            % os.path.basename(parameters.input_bam)
            #% (describe_files(parameters.input_bams),)
        PicardNode.__init__(self,
                            command=parameters.command.finalize(),
                            description=description,
                            dependencies=parameters.dependencies)


